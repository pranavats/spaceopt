* Binary Rewriting Process
** Jargon
   1. CTI (Direct & Indirect): 
      Control Transfer Instructions. eg. =jmp=.
      - Direct CTIs contain the address as immediate operand. eg. =jmp 0x40503829=.
      - Indirect CTIs contain the pointers to jump-address. eg. =jmp eax=.
   2. ACP (Address Creation Point): Locations of addresses whose targets(data/code) may need movement during rewrite process. The point refers perhaps to the initialization of a Pointer (register/memory) with jump-address.
   3. AUP(Address Usage Point): Positions in code where the Indirect CTIs are invoked.
   4. DU Chain: A data structure where each node contains instruction which Defines(ACP) & Uses(AUP, in a CTI).
** Problems
   - Code Relocation
     - Discriminating addresses from immediate data when updating the new ACPs(could be a loop bound)
       Since binaries do not contain any information about operand types.
       In either case, rewrite could be /incomplete/ or /wrong/.
       - Workaround: [Usage Point Translation] Instead of ACPs, only modify (each) AUP, leave initial pointer initialization as it is. A /translator/ assigns the correct modified address to the pointer. eg. the indirect CTI =jmp rax=, after replaced by the translator in some intermediate language looks like:
       #+BEGIN_SRC C
         if (rax == 0x40458754)
             mod_rax = new_address;
         else if (rax == 0x40648790)
             mod_rax = some_address;
         jmp mod_rax
       #+END_SRC
         Every Possible target of =jmp rax= has been implemented in the translator, to guarantee correct control transfer.
         The entire code segment is also preserved in case it contains any embedded data target for data pointers.
        - Solution: Using heuristics defined in section 4 of [[https://www.usenix.org/system/files/conference/usenixsecurity15/sec15-paper-wang-shuai.pdf][Reassembleable Disassembling]]

            Easy: Identifying code to code and code to data references
                1. The immediate value must fall in range of address space of the binary.
                    Above heurisitc above is enough to handle c2c and c2d references.

            Difficult: Identifying Data to code and Data to Data references
            Problem in handling diffcult parts: slicing of data section which is a continuous bytestream.

                2.  Symbol references in data sections are n byte aligned (n=4 for 32 bit binaries and 8 for 64 bit binaries).
                    A sliding window is maintained which is n bytes long, if the value within window is acceptable then window is advanced by n bytes, otherwise by 1 byte.
                3. Users won't modify data sections of binaries.
                    Assuming this, we don't need to touch d2d references.
                4. If an address resides in data section then it must be either a function pointer or jump table entry.
            Consult the paper for jump table identification and additional details.


   - Data Relocation
     Prohibited. Do not move data targets. Preserve the original data segment
   - Code Discovery (During Disassembly, problem in separating code from Embedded data)
     - Data maybe embedded in the code as
       - [[https://en.wikipedia.org/wiki/Branch_table][jump/branch tables]] for multiway branching
       - Padding Bytes
       - [[https://stackoverflow.com/questions/2846914/what-is-meant-by-memory-is-8-bytes-aligned#2846925][Alignment Bytes]]
     - Algorithms
       - [ ] Linear Sweep: Simply disassemble linearly until any invalid instruction is encountered.
       - [x] Recursive Traversal Disassembly.
   - AUPs outside the scope of binary (~Callback~ from an outside binary loaded at run time)
     - The AUP might lie outside the bounds of binary, & a function pointer may be passed as a parameter to the foreign function (Dynamically linked), this would perhaps be an ACP.
     - Since the parameter could be an arbitrary constant or a function pointer, keep the parameter intact, maintaining correctness in either case, if it's a function pointer, & is used to callback in our binary. control will transfer to redundant code segment, hence ensuring correctness.
       - Solution (Partial):
         - Maintain a DU Chain, if we modify a define when moving a snippets to common area, update their uses.
           (Requires Indirect CTI detection to be 100% accurate, or bugs might follow.)
** In Linkers, Conventionally
   Absolute addresses cannot be computed until the linking phase, so compiler generates a relocation entry wherever absolute addresses are required.
   Note: x86 doesn't allow PC-relative indirect addressing, so relocation entry list is sufficient to obtain all absolute addresses in that case. [TODO] What about other Architectures?
