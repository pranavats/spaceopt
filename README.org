* Space optimization in Binaries
  When redundant instruction chunks exist in a program they not only take up more space, but are also expected to compete for space in cache memory, leading to more cash misses.

  Eliminating the redundancy by combining all of them in a way that they still retain their behaviour, may reduce binary size, & competition for cache at the same time.

* Tasks
  - [-] Analyze various Dynamic Binary Instrumentation [[./doc/analysis/tools/list.org][tools]].
    - Follow basic tutorials & read documentation for each.
  - [-] Analyze the problem with static analysis.
  - [-] Analyze the existence/frequency of redundancy in large code bases (Apache http server).
* Strategic Issues
  - [[./doc/analysis/static-analysis/static-analysis.org][Static Analysis]]([[./doc/analysis/static-analysis/rewrite-process.org][Details]])
    - Not feasible if relocation information unavailable, and not novel otherwise.
  - Dynamic Analysis
    - Time overhead at run time.
    - Acts as an external dependency at run time.
  - Hybrid(Details TODO)
    First dynamically analyze the control flow of the program in the /test run/, and then generate a rewritten binary for that specific hardware, using the information about control flow gathered in the previous step. 
    - A test run may not determine correct flow of a non-trivial program.
      - Very difficult to automate such a test run.
      - Limitations on knowledge about the binary would hinder even manual extensive testing of all features.
* Related Works
  - Papers

    On static rewriting of binaries without relocation information.
    - [[http://www.ece.umd.edu/~barua/without-relocation-technical-report10.pdf][Binary Rewriting without Relocation Information]] (Technical Report)
    - [[http://www.ece.umd.edu/~barua/smithson-WCRE-2013.pdf][Static Binary Rewriting without Supplemental Information]] (Paper)
